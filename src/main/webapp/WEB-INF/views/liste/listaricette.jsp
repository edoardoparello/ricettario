<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>

<ul id="listaricette" class="list-group">
		<c:forEach var="ricette" items="${collection}">
			<li class="list-group-item"><a href="/masterchef/listaricette/ricetta/${ricette.id}">${ricette.ingrediente.nome}</a>   
				<a href="/masterchef/listaricette/remove/${ricette.id}" class="remove"><span class="glyphicon glyphicon-trash pull-right" aria-hidden="true" ></span></a>
				<a href="#" ><span class="glyphicon glyphicon-pencil pull-right" aria-hidden="true" ></span></a>
		</c:forEach>
		
	
</ul>