<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Le Ricette dello Chef - Admin</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
</head>
<body>
	<div class="jumbotron" align="center">
	<div id="header" class="main" style="font-size:32pt">
	
		<h1>
		<a href="/masterchef/"><span class="glyphicon glyphicon-apple" aria-hidden="true" ></span></a><a href ="#" onclick="window.location.reload(true);"> Ricette</a> </h1>
		</div>
		<div class="container">
		<div class=row>
			<div class="menu" style="background: #CFCFCF;">
			<ul class="nav nav-pills">
				<li role="presentation" class="active"><a href="#">Ricette</a></li>
				<li role="presentation"><a href="ricette/listaingredienti">Ingredienti</a></li>
				<li role="presentation"><a href="/masterchef/listaricette/new">Aggiungi Ricetta</a></li>
			</ul>
			<div id="main" class="main">
			<jsp:include page="../liste/listaricette.jsp"/>
			</div>
		</div>
		</div>
		</div>
	</div>
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src='<c:url value="/resources/js/ricette.js"/>'></script>
</body>
</html>