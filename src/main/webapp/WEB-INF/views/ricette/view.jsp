<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>

<%@ page session="false"%>
<!DOCTYPE html >
<html>
<head>


<title>${ricette.ingrediente.nome}</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"
	integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ=="
	crossorigin="anonymous">

</head>
<body>

	
	<h1 align="center">	${ricette.ingrediente.nome}</h1>
	<div class="container">
	<div class="jumbotron">
	
	<div><p><c:forEach items="${ricette.categoria}" var="ricette"> 
				Categoria : ${ricette.nome}
    </c:forEach>   </p></div>
	
	
	<div><p>Tempo di Preparazione : ${ricette.tempo}</p></div>


			<div>
				<c:forEach items="${ricette.dosi}" var="ricette">
					<table>
						<tr>
							<td>Ingrediente:</td>
							<td>${ricette.key.nome}</td>
						</tr>
						<tr>
							<td>Dose:</td>
							<td>${ricette.value}</td>
						</tr>
					</table>
				</c:forEach>
			</div>




			<div>Preparazione : ${ricette.preparazione}</div>
	<div>Livello difficoltà : ${ricette.difficolta}</div>
	
	</div>
		<a href="/masterchef/listaricette/" class="btn btn-primary">Torna alla Lista</a>
	</div>


</body>
</html>
