<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
<title>Insert a new Recipe</title>
</head>
<body>
	<div class="input-group" >
	<f:form action="new" method="POST" modelAttribute="ricette">
		<f:input type="hidden" path="id" value="${ricette.id}"></f:input>
		<div><f:select  path="ingrediente.id" placeholder="Seleziona ingrediente base">
				<option value="">Seleziona ingrediente base</option>
			<c:forEach var="ing" items="${ingrediente}">
				<f:option value="${ing.id}">${ing.nome}</f:option>
				</c:forEach>
		</f:select></div>
		<div><f:select  path="categoria" placeholder="Seleziona categoria" multiple="no" >
				<option value="">Seleziona la categoria</option>
			<c:forEach var="cat" items="${categoria}">
				<f:option value="${cat.id}">${cat.nome}</f:option>
				</c:forEach>
		</f:select></div>
		
		<div><f:select  path="dosi" placeholder="Seleziona ingrediente" multiple="no">
				<option value="">Seleziona ingrediente</option>
			<c:forEach var="ing" items="${ingrediente}">
				<f:option value="${ing.id}">${ing.nome}</f:option>
				</c:forEach>
		</f:select>
		<f:input type="text" path="dosi" value="${ricette.dosi.value}"></f:input>
		
		</div>
		
		
		<div><f:select path="difficolta" placeholder="Seleziona difficoltà">
				<option value="">Seleziona la difficoltà</option>
				<f:option value="1">1</f:option>
				<f:option value="2">2</f:option>
				<f:option value="3">3</f:option>
				
		</f:select></div>
		<div><f:textarea type="text" path="preparazione" value="${ricette.preparazione}" rows="10" cols="100" class="form-control" placeholder="Preparazione"></f:textarea></div>
		<div>Tempo: <f:input type="text" path="tempo" value="${ricette.tempo}" placeholder="Tempo"/></div>
		
		<input type="submit" />
	</f:form>
	</div>
</body>
</html>