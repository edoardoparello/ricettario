package it.parello.masterchef.interfaces;

import java.util.List;

import it.parello.masterchef.models.Categoria;
import it.parello.masterchef.models.Ingrediente;
import it.parello.masterchef.models.Ricette;

public interface IService {
	public List<Ricette> elencaRicette();
	public Ricette addRicetta(Ricette ricette);
	public Ricette getRicetta(int id);
	public boolean rimuoviRicetta(int id);
	public List<Categoria> elencaCategorie();
	public List<Ingrediente> elencaIngredienti();
}
