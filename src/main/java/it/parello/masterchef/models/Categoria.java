package it.parello.masterchef.models;



import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="categorie")
public class Categoria {
	

	@Id
	@Column(name = "id")
	private int id;
	
	@Column(name="nome")
	private String nome;
	
	@ManyToMany(mappedBy="categoria",cascade=CascadeType.ALL)
	/*@ManyToMany(fetch=FetchType.LAZY,cascade= CascadeType.ALL)
	@JoinTable(name="ricette_categorie",  
    joinColumns={@JoinColumn(name="id_categoria" , nullable = false,updatable=false)},
    inverseJoinColumns = { @JoinColumn(name = "id_ricetta")})*/
	private List<Ricette> ricette;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public List<Ricette> getRicette() {
		return ricette;
	}
	public void setRicette(List<Ricette> ricette) {
		this.ricette = ricette;
	}
	
	
	
}
