package it.parello.masterchef.models;

import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyClass;
import javax.persistence.MapKeyColumn;
import javax.persistence.Table;

@Entity
@Table(name="ricette")
public class Ricette{
	

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;

	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name= "id_ingrediente",referencedColumnName="id")
	private Ingrediente ingrediente;
	
	
	@ElementCollection(fetch = FetchType.LAZY)
	@MapKeyClass(Ingrediente.class)
	private Map<Ingrediente,String> dosi;
	
	@Column(name="preparazione")
	private String preparazione;
	
	@Column(name="tempo")
	private long tempo;
	
	@Column(name="difficolta")
	private int difficolta;
	
	 
	@ManyToMany(fetch=FetchType.LAZY,cascade= CascadeType.ALL)
	@JoinTable(name="ricette_categorie",  
    joinColumns={@JoinColumn(name="id_ricetta")},
    inverseJoinColumns = { @JoinColumn(name = "id_categoria" , nullable = false,updatable=false)})
	//@ManyToMany(mappedBy="ricette",cascade=CascadeType.ALL)
	private List<Categoria> categoria;
	
		

	public Ricette(int id,Map<Ingrediente,String> dosi, Ingrediente ingrediente, String preparazione, long tempo, int difficolta, List<Categoria> categoria) {
		this.id = id;
		this.ingrediente=ingrediente;
		this.dosi=dosi;
		this.preparazione = preparazione;
		this.tempo = tempo;
		this.difficolta = difficolta;
		this.categoria = categoria;
	}
	public Ricette() {
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
		
	public Ingrediente getIngrediente() {
		return ingrediente;
	}
	public void setIngrediente(Ingrediente ingrediente) {
		this.ingrediente = ingrediente;
	}
	public Map<Ingrediente, String> getDosi() {
		return dosi;
	}
	public void setDosi(Map<Ingrediente, String> dosi) {
		this.dosi = dosi;
	}
	public String getPreparazione() {
		return preparazione;
	}
	public void setPreparazione(String preparazione) {
		this.preparazione = preparazione;
	}
	public long getTempo() {
		return tempo;
	}
	public void setTempo(long tempo) {
		this.tempo = tempo;
	}
	public int getDifficolta() {
		return difficolta;
	}
	public void setDifficolta(int difficolta) {
		this.difficolta = difficolta;
	}
	
	
	public List<Categoria> getCategoria() {
		return categoria;
	}
	public void setCategoria(List<Categoria> categoria) {
		this.categoria = categoria;
	}

	
}
