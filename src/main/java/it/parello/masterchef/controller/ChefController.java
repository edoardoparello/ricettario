package it.parello.masterchef.controller;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.parello.masterchef.models.Categoria;
import it.parello.masterchef.models.Ingrediente;
import it.parello.masterchef.models.Ricette;




@Controller
public class ChefController {
	
	@Autowired
	private ChefService service;

	private static final Logger logger = LoggerFactory.getLogger(ChefController.class);
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		//binder.setDisallowedFields("dosi");
		//binder.setDisallowedFields("categoria");
	}
	
	
	@RequestMapping("/listaricette")
	public String Ricette(Model m){
		List<Ricette> ricette = service.elencaRicette();
		m.addAttribute("collection", ricette);
		return "ricette/ricette";
	}
	
	@RequestMapping(value="/listaricette/ricetta/{id}", method=RequestMethod.GET)
	public String getRicetta(Model model, @PathVariable("id")int id) {
		Ricette ricetta = service.getRicetta(id);
		model.addAttribute("ricette", ricetta);
		return "ricette/view";
	}
	
	@RequestMapping(value="/listaricette/remove/{id}", method=RequestMethod.POST)
	public String remove(Model m, @PathVariable("id")int id) {
		service.rimuoviRicetta(id);
		List<Ricette> ricette = service.elencaRicette();
		m.addAttribute("collection", ricette);
		return "liste/listaricette";
	}
	
	@RequestMapping(value = "/listaricette/new", method = RequestMethod.GET)
	public String newRicetta(Model m) {
		List<Categoria> categorie = new ArrayList<Categoria>();
		categorie=service.elencaCategorie();
		List<Ingrediente> ingredienti = new ArrayList<Ingrediente>();
		ingredienti=service.elencaIngredienti();
		m.addAttribute("categoria",categorie);
		m.addAttribute("ingrediente", ingredienti);
		m.addAttribute("ricette", new Ricette());
		return "ricette/new";
	}

	@RequestMapping(value = "/listaricette/new", method = RequestMethod.POST)
	public String createRicetta(@ModelAttribute Ricette ricette, BindingResult binder) {
		service.addRicetta(ricette);
		return "redirect:/listaricette";
	}
	
	@RequestMapping("/")
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		String formattedDate = dateFormat.format(date);
		model.addAttribute("serverTime", formattedDate );
		return "home";
	}
	
	
	
}
