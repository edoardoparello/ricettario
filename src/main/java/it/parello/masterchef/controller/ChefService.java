package it.parello.masterchef.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.parello.masterchef.interfaces.IService;
import it.parello.masterchef.models.Categoria;
import it.parello.masterchef.models.Ingrediente;
import it.parello.masterchef.models.Ricette;



@Service
public class ChefService implements IService{
	
	@Autowired
	private ChefRepository repository;
	

	@Override
	public List<Ricette> elencaRicette() {
		return repository.elencaRicette();
	}
	@Override
	public Ricette addRicetta(Ricette ricette) {
		return repository.addRicetta(ricette);
		
	}
	@Override
	public Ricette getRicetta(int id) {
		return repository.getRicetta(id);
	}
	@Override
	public boolean rimuoviRicetta(int id) {
		return repository.rimuoviRicetta(id);
		
	}
	@Override
	public List<Categoria> elencaCategorie() {
		return repository.elencaCategorie();
	}
	@Override
	public List<Ingrediente> elencaIngredienti() {
		return repository.elencaIngredienti();
	}

	

	

	
	
}
