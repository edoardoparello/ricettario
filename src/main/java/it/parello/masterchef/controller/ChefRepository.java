package it.parello.masterchef.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import it.parello.masterchef.interfaces.IRepository;
import it.parello.masterchef.models.Categoria;
import it.parello.masterchef.models.Ingrediente;
import it.parello.masterchef.models.Ricette;

@Repository
public class ChefRepository implements IRepository{

	
	@Autowired
	private EntityManagerFactory entityManagerFactory;
	
	
	
	
	public Ricette getRicetta(int id) {
		EntityManager em = entityManagerFactory.createEntityManager();
		Ricette ricetta = (Ricette) em.find(Ricette.class, id);
			Hibernate.initialize(ricetta.getCategoria());
			Hibernate.initialize(ricetta.getDosi());
		em.close();
		return ricetta;
	}
	
	
	public List<Ricette> elencaRicette() {
		EntityManager em = entityManagerFactory.createEntityManager();
		List<Ricette> listaRicette = em.createQuery("from Ricette").getResultList();
		em.close();
		return listaRicette;
	}
	
	

	public Ricette addRicetta(Ricette ricette) {
		EntityManager em = entityManagerFactory.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		Ricette ricetta = null;
		try{
		ricetta = em.merge(ricette);
		em.flush(); // svuota la cache
		tx.commit();
		}catch(Exception e){
			tx.rollback();
		}
		em.close();

		return ricetta;
	}


	public boolean rimuoviRicetta(int id) {
		EntityManager em = entityManagerFactory.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		Ricette ricetta = (Ricette) em.find(Ricette.class, id);
		em.remove(ricetta);
		em.flush(); // svuota la cache
		tx.commit();
		em.close();
		return true;
	}


	public List<Categoria> elencaCategorie() {
		EntityManager em = entityManagerFactory.createEntityManager();
		List<Categoria> listaCategorie = em.createQuery("from Categoria").getResultList();
		em.close();
		return listaCategorie;
	}


	public List<Ingrediente> elencaIngredienti() {
		EntityManager em = entityManagerFactory.createEntityManager();
		List<Ingrediente> listaIngredienti = em.createQuery("from Ingrediente").getResultList();
		em.close();
		return listaIngredienti;
	}


	


	


	
}
